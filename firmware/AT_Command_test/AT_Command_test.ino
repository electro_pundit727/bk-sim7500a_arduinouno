/* This code allows you to freely test AT commands by entering them into the serial monitor! 
 * As to the specific operation, you can use following string commands with AT Commands. 
 *   
 *   ### string commands ###            ### AT commnads ###
 *   
 *   - "ON" 
 *   - "OFF"                           - AT+CPOF
 *   - "RESET"                         - AT+CRESET       
 *   - "FACTORY"                       - AT&F0 
 *   - "BAUD<baudrate>"                - AT+IPREX=baudrate
 *   
 *   ### HTTP Request     AT+CHTTPACT="<Address>",<port>  
 *                          <address>: The HTTP server domain name or IP address.
 *                          <port>: The HTTP server port.
 *                          
 *     ex) AT+CHTTPACT=”www.mywebsite.com”,80    
 *         Responses:         
                          +CHTTPACT: REQUEST
                          +CHTTPACT: DATA, <len>
                          …
                          +CHTTPACT: DATA, <len>
                          …
                          …                        
                          +CHTTPACT: 0
                  -----------------------------------        
                          +CME ERROR
                  -----------------------------------        
                          +CHTTPACT: <err>
                          ERROR
                  -----------------------------------        
                          +CHTTPACT: REQUEST
                          +CHTTPACT: <err>
                          ERROR
                  -----------------------------------        
                          +CHTTPACT: REQUEST
                          +CHTTPACT: DATA, <len>
                          …
                          +CHTTPACT: DATA, <len>
                          …
                          …
                          +CHTTPACT: <err>
                          ERROR
 */

#include <SoftwareSerial.h>

#define SIMCOM_7500

// For BK-SIM7500A shield
#define PWRKEY 5
#define SLEEP 3
#define SIM_TX 10     // Microcontroller RX
#define SIM_RX 11     // Microcontroller TX

SoftwareSerial fonaSS = SoftwareSerial(SIM_TX, SIM_RX);

uint32_t baudRate;

void setup() {
  Serial.begin(115200);
  Serial.println("*****AT Command Test*****");

  pinMode(SLEEP, OUTPUT);
  pinMode(PWRKEY, OUTPUT);

  // Power on the module
  powerOn();
  delay(4000); // Let the shield power up

  // Hard-code baud rate
  // Can use the "BAUD" command to switch baud rate at any time in case of a baud rate mismatch.

  fonaSS.begin(115200); // Default baud rate
  // fonaSS.begin(4800);
}

void loop() {
  if (Serial.available()) {
    String userCmd = Serial.readString();

    if (userCmd == "ON") powerOn();
    else if (userCmd == "OFF") powerOff();
    else if (userCmd == "RESET") FONAreset();
    else if (userCmd == "FACTORY") facReset();
    else if (userCmd.startsWith("BAUD")) {
      baudRate = userCmd.substring(4).toInt();
      Serial.print("*** Switching to "); Serial.print(baudRate); Serial.println(" baud");
      Serial.print(" --> ");
      fonaSS.println("AT+IPREX=" + String(baudRate));
      delay(100); // Give it some time to chill
      fonaSS.begin(baudRate);
    }
    else {
      Serial.print(" --> ");
      Serial.print(userCmd);
      fonaSS.println(userCmd);
    }
  }

  if (fonaSS.available()) {
    Serial.write(fonaSS.read());
  }
}

void flushInput() {
  while (Serial.available()) {
    char c = Serial.read(); // Flush pending data
  }
}

void facReset() {
  // This function factory resets the SIM7000
  Serial.println("*** Factory resetting...");
  Serial.print(" --> AT&F0");
  fonaSS.println("AT&F0");
}

void powerOn() {
  // Turn on the module by pulsing PWRKEY low for a little bit
  // This amount of time depends on the specific module that's used
  Serial.println("*** Turning ON (takes about 4.2s)");
  digitalWrite(PWRKEY, LOW);
#if defined(SIMCOM_7500)
  delay(500);     // at least 100ms for SIM7500
#endif
  digitalWrite(PWRKEY, HIGH);
}

void powerOff() {
  Serial.println("*** Turning OFF (takes about 1.3s)");
  // fonaSS.println("AT+CPOF");  // AT command to power off the module

  digitalWrite(FONA_PWRKEY, LOW);
#if defined(SIMCOM_7500)
  delay(3000);   // At least 2.5s for SIM7500
#endif
  digitalWrite(FONA_PWRKEY, HIGH);
}

void FONAreset() {
  Serial.println("*** Module resetting...");
  Serial.print(" --> AT+CRESET");
  fonaSS.println("AT+CRESET");
}

