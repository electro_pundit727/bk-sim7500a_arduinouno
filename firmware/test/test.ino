/***************************************************
  This is an sketch to test a simple GET, POST request.
  These cellular modules use TTL Serial to communicate, 2 pins are required to interface.
 ****************************************************/

#include "Adafruit_FONA.h"
#include <SoftwareSerial.h>

#define SIMCOM_7500

#define PWRKEY 5
#define SLEEP 3
#define SIM_TX 10 // Microcontroller RX
#define SIM_RX 11 // Microcontroller TX

SoftwareSerial fonaSS = SoftwareSerial(SIM_TX, SIM_RX);
SoftwareSerial *fonaSerial = &fonaSS;

Adafruit_FONA_LTE fona = Adafruit_FONA_LTE();

uint8_t readline(char *buff, uint8_t maxbuff, uint16_t timeout = 0);
uint8_t type;
char imei[16] = {0}; // MUST use a 16 character buffer for IMEI!

void setup() {

  pinMode(SLEEP, OUTPUT);
  pinMode(PWRKEY, OUTPUT);
  // Turn on the module by pulsing PWRKEY low for a little bit
  // This amount of time depends on the specific module that's used
  powerOn();

  Serial.begin(115200);
  Serial.println(F("FONA basic test"));
  Serial.println(F("Initializing....(May take 3 seconds)"));

  //fona.setGPRSNetworkSettings(F("Aeris SIM crad")); // For Aeris IoT SIM card

  // Optionally configure HTTP gets to follow redirects over SSL.
  // Default is not to follow SSL redirects, however if you uncomment
  // the following line then redirects over SSL will be followed.
  // fona.setHTTPSRedirect(true);

#ifdef SIMCOM_7500
  fonaSerial->begin(115200); // Default LTE shield baud rate
  fona.begin(*fonaSerial); // Don't use if  statement because an OK reply could be sent incorrectly at 115200 baud

  Serial.println(F("Configuring to 4800 baud"));
  fona.setBaudrate(4800); // Set to 4800 baud
  fonaSerial->begin(4800);
  if (!fona.begin(*fonaSerial)) {
    Serial.println(F("Couldn't find FONA"));
    while (1); // Don't proceed if it couldn't find the device
  }

  Serial.println(F("Configuring to 4800 baud"));
  fonaSS.println("AT+IPR=4800"); // Set baud rate temporarily
  fonaSS.begin(4800);
  if (!fona.begin(fonaSS)) {
    Serial.println(F("Couldn't find FONA"));
    while (1); // Don't proceed if it couldn't find the device
  }
#endif

  type = fona.type();

  // Print module IMEI number.
  uint8_t imeiLen = fona.getIMEI(imei);
  if (imeiLen > 0) {
    Serial.print("Module IMEI: "); Serial.println(imei);
  }
}

void loop() {

  Serial.print(F("Request: "));   

  while (!Serial.available()) {
    if (fona.available()) {
      Serial.write(fona.read());
    }
  }
  char command = Serial.read();
  Serial.println(command);

  switch (command) {
    case 'G': {
        PostGetRequest(); break;
      }
    case 'P': {
        PostDataRequest(); break;
      }
    case 'I': {
        GetNetworkInfo(); break;
      }
    default: {
        Serial.println(F("Unknown command"));
        break;
      }
  }
  flushSerial();
  while (fona.available()) {
    Serial.write(fona.read());
  }
}

void PostGetRequest() {

  // read website URL
  uint16_t statuscode;
  int16_t length;
  char url[100];

  flushSerial();
  Serial.println(F("Enter the Website URL to read!"));
  Serial.println(F("URL to read (e.g. www.adafruit.com/testwifi/index.html):"));
  Serial.print(F("http://")); readline(url, 79);
  Serial.println(url);

  Serial.println(F("****"));
  if (!fona.HTTP_GET_start(url, &statuscode, (uint16_t *)&length)) {
    Serial.println("Failed!");
  }
  else {
    while (length > 0) {
      while (fona.available()) {
        char c = fona.read();

        // Serial.write is too slow, we'll write directly to Serial register!
#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
        loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
        UDR0 = c;
#else
        Serial.write(c);
#endif
        length--;
        if (! length) break;
      }
    }
    Serial.println(F("\n****"));
    fona.HTTP_GET_end();
  }
}
void PostDataRequest() {

  // Post data to website
  uint16_t statuscode;
  int16_t length;
  char url[100];
  char data[1000];

  flushSerial();
  Serial.println(F("Please enter the Website URL to post the data!"));
  Serial.println(F("URL to post (e.g. httpbin.org/post):"));
  Serial.print(F("http://"));
  readline(url, 99);
  Serial.println(url);
  Serial.println(F("Data to post (e.g. \"foo\" or \"{\"simple\":\"json\"}\"):"));
  readline(data, 99);
  Serial.println(data);

  Serial.println(F("****************************************"));
  if (!fona.HTTP_POST_start(url, F("text/plain"), (uint8_t *) data, strlen(data), &statuscode, (uint16_t *)&length)) {
    Serial.println("Failed!");
  }
  while (length > 0) {
    while (fona.available()) {
      char c = fona.read();

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
      loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
      UDR0 = c;
#else
      Serial.write(c);
#endif

      length--;
      if (! length) break;
    }
  }
  Serial.println(F("\n****"));
  fona.HTTP_POST_end();
}

void GetNetworkInfo() {

  // Get connection type, cellular band, carrier name, etc.
  fona.getNetworkInfo();
}

void flushSerial() {

  // Read the Serial port.
  while (Serial.available())
    Serial.read();
}

uint8_t readline(char *buff, uint8_t maxbuff, uint16_t timeout) {

  // Get the url to send(or to read) from the serial monitor.
  uint16_t buffidx = 0;
  boolean timeoutvalid = true;
  if (timeout == 0) timeoutvalid = false;

  while (true) {
    if (buffidx > maxbuff) {
      //Serial.println(F("SPACE"));
      break;
    }

    while (Serial.available()) {
      char c =  Serial.read();

      //Serial.print(c, HEX); Serial.print("#"); Serial.println(c);

      if (c == '\r') continue;
      if (c == 0xA) {
        if (buffidx == 0)   // the first 0x0A is ignored
          continue;

        timeout = 0;         // the second 0x0A is the end of the line
        timeoutvalid = true;
        break;
      }
      buff[buffidx] = c;
      buffidx++;
    }

    if (timeoutvalid && timeout == 0) {
      //Serial.println(F("TIMEOUT"));
      break;
    }
    delay(1);
  }
  buff[buffidx] = 0;  // null term
  return buffidx;
}

void powerOn() {

  // Power on the module
  digitalWrite(PWRKEY, LOW);
#if defined(SIMCOM_7500)
  delay(500); // For SIM7500
#endif
  digitalWrite(PWRKEY, HIGH);
}

void SleepMode() {

  // SIM7500A module go to the sleep mode
  digitalWrite(SLEEP, LOW);
#if defined(SIMCOM_7500)
  delay(500); // For SIM7500
#endif
  digitalWrite(SLEEP, HIGH);

}

//  char command;
//  while (Serial.available() > 0) {
//    // get the request type.
//    char c =  Serial.read();
//    if (c == '\n') break;
//    command = c;
//  }
//  Serial.println(command);
